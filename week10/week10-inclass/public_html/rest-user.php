<?php
require_once('../inc/Users.class.php');

$user = new Users();

$userData = "";
$userList = "";

if (isset($_GET['userID']) && $_GET['userID'] > 0)
{
    if ($user->load($_GET['userID']))
    {
        $userData = $user->data;
        
        $userData = json_encode($userData);
    }
    echo $userData;
    //var_dump($userData);
} 
else 
{
    // web service to pull a list of users
    $userList = $user->getList(
        null, null,
        (isset($_GET['filterColumn']) && isset($_GET['filterText']) ? $_GET['filterColumn'] : null),
        (isset($_GET['filterColumn']) && isset($_GET['filterText']) ? $_GET['filterText'] : null)
    );

    if (is_array($userList))
    {
        $userList = json_encode($userList);
    }
    echo $userList;
}
?>