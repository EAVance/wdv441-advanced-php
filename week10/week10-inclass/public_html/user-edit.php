<?php
// usage: http://localhost:8080/WDV441_2018/week05/public_html/article-edit.php?user_id=1
// usage new: http://localhost:8080/WDV441_2018/week05/public_html/article-edit.php
require_once('../inc/Users.class.php');

$user = new Users();

$userDataArray = array();
$usersErrorsArray = array();

// load the article if we have it
if (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] > 0) 
{
    $user->load($_REQUEST['user_id']);
    $userDataArray = $user->data;
}

if (isset($_POST['Cancel'])) 
{
    header("location: user-list.php");
    exit;
}

// apply the data if we have new data
if (isset($_POST['Save']))
{
    $userDataArray = $_POST;
    // sanitize
    $userDataArray = $user->santinize($userDataArray);
    $user->set($userDataArray);
    
    // validate
    if ($user->validate())
    {
        // save
        if ($user->save())
        {
            header("location: user-save-success.php");
            exit;
        }
        else
        {
            $usersErrorsArray[] = "Save failed";
        }
    }
    else
    {
        $usersErrorsArray = $user->errors;
        $userDataArray = $user->data;
    }
}

require_once('../tpl/user-edit.tpl.php');
?>