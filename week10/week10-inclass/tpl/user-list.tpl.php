<html>
    <body>
        <div>System Users - <a href="/WDV441_2018/week08/public_html/user-edit.php">Add New User</a></div>        
        <div>
            <form action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" method="GET">
                Search: 
                <select name="filterColumn">
                    <option value="username">username</option>
                    <option value="user_level">user level</option>
                </select>
                &nbsp;<input type="text" name="filterText"/>
                &nbsp;<input type="submit" name="filter" value="Search"/>
            </form>
        </div>
        <div>
            <div style="clear:both;">
                <div style="float:left; border:1px solid black;">Article Title</div>
                <div style="float:left; border:1px solid black;">Article Author</div>
                <div style="float:left; border:1px solid black;">Article Date</div>
                <div style="float:left; border:1px solid black;">&nbsp;</div>
                <div style="float:left; border:1px solid black;">&nbsp;</div>
            </div>
            <?php foreach ($userList as $userData) 
            { ?>
                <div style="clear:both;">
                    <div style="float:left; border:1px solid black;"><?php echo $userData['username']; ?></div>
                    <div style="float:left; border:1px solid black;"><?php echo $userData['user_level']; ?></div>
                    <div style="float:left; border:1px solid black;"><a href="/WDV441_2018/week08/public_html/user-edit.php?user_id=<?php echo $userData['user_id']; ?>">Edit</a></div>
                    <div style="float:left; border:1px solid black;"><a href="/WDV441_2018/week08/public_html/user-view.php?user_id=<?php echo $userData['user_id']; ?>">View</a></div>
                </div>
            <?php } ?>                
            <br><br>
            <table border="1">
                <tr>
                    <th>username&nbsp;-&nbsp;<a href="<?php echo $_SERVER['SCRIPT_NAME']; ?>?sortColumn=username&sortDirection=ASC">A</a>&nbsp;<a href="<?php echo $_SERVER['SCRIPT_NAME']; ?>?sortColumn=username&sortDirection=DESC">D</a></th>
                    <th>user level&nbsp;-&nbsp;<a href="<?php echo $_SERVER['SCRIPT_NAME']; ?>?sortColumn=user_level&sortDirection=ASC">A</a>&nbsp;<a href="<?php echo $_SERVER['SCRIPT_NAME']; ?>?sortColumn=user_level&sortDirection=DESC">D</a></th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                </tr>
                <?php foreach ($userList as $userData) 
                { ?>
                    <tr>
                        <td><?php echo $userData['username']; ?></td>                
                        <td><?php echo $userData['user_level']; ?></td>
                        <td><a href="/WDV441_2018/week08/public_html/user-edit.php?user_id=<?php echo $userData['user_id']; ?>">Edit</a></td>
                        <td><a href="/WDV441_2018/week08/public_html/user-view.php?user_id=<?php echo $userData['user_id']; ?>">View</a></td>
                            
                    </tr>
                <?php } ?>                
            </table>
        </div>
    </body>
</html>