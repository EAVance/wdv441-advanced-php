<html>
    <body>
        title: <?php echo (isset($articleDataArray['articleTitle']) ? $articleDataArray['articleTitle'] : ''); ?><br>
        content: <?php echo (isset($articleDataArray['articleContent']) ? $articleDataArray['articleContent'] : ''); ?><br>
        author: <?php echo (isset($articleDataArray['articleAuthor']) ? $articleDataArray['articleAuthor'] : ''); ?><br>
        date: <?php echo (isset($articleDataArray['articleDate']) ? $articleDataArray['articleDate'] : ''); ?><br>        
        <br>
        <?php if (file_exists(dirname(__FILE__) . "/../public_html/images/article_" . $articleDataArray['articleID'] . ".jpg"))
        { ?>
            <img src="images/article_<?php echo $articleDataArray['articleID'] . ".jpg"; ?>"/>
        <?php }?>
        <a href="article-list.php">Back to Article List</a>
    </body>
</html>