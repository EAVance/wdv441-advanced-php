<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>User Login</title>
        <style>
            form {
                padding:2%;
                background-color:#eeeeee;
                width:70%;
                margin:0 auto;
                min-width:375px;
            }
            h2 {
                text-align:center;
            }
            p {
                width:350px;
                margin:0 auto;
            }
            #errors {
                text-align:center;
                color:#D06666;
            }
            .invalidUser {
                text-align:center;
                padding-bottom:2%;
                font-size:1.1em;
            }
            input[type="text"],input[type="password"]{
                display:block;
                width:100%;
                border:1px solid #dfdfdf;
                border-radius:5px;
                padding:2%;
            }
            input::placeholder {
                font-style:italic;
                color:#d6d6d6;
            }
            #formBtns{
                text-align:center;
            }
            input[type="submit"]{
                font-size:1.3em;
                margin:1%;
                padding:2%;
            }
        </style>
    </head>
    <body>
        <form action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" method="post">
            <h2>Login</h2>
            <p class="invalidUser">
                <span id="errors"><?php echo echoValue($userErrorsArray, 'invalidUser');?></span> 
            </p>
            <p>Username (<em>email</em>): 
                <?php if (isset($userErrorsArray['username'])) 
                { ?>
                    <span id="errors"><?php echo $userErrorsArray['username'];?></span> 
                <?php } ?>
                <input type="text" name="username" placeholder="abc@yahoo.com" value="<?php echo echoValue($userDataArray, 'username'); ?>"/><br>
            </p>
            <p>Password: 
                <?php if (isset($userErrorsArray['password'])) 
                { ?>
                    <span id="errors"><?php echo $userErrorsArray['password'];?></span> 
                <?php } ?>
                <input type="password" name="password" value="<?php echo echoValue($userDataArray, 'password'); ?>"/><br>
            </p>
            <div id="formBtns">
                <input type="submit" name="Login" value="Login"/>
                <input type="submit" name="Cancel" value="Cancel"/> 
            </div>           
        </form>        
    </body>
</html>