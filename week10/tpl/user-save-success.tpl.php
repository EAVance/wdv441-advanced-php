<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>User-Saved</title>
        <style>
            #container {
                padding:2%;
                background-color:#eeeeee;
                width:70%;
                margin:0 auto;
                min-width:375px;
                letter-spacing:.6px;
            }
            h2,h3 {
                text-align:center;
            }
            h3 {
                padding:3%;
            }
            h3 a,.editLink,.viewLink {
                color:#648196;
                text-decoration:none;
            }
            h3 a:hover,.editLink:hover,.viewLink:hover {
                color:#3c4d5a;
            }
            .listIcon {
                font-size:1.4em;
            }
            .checkmark {
                color:#005b00;
            }
        </style>
    </head>
    <body>
        <div id="container">
            <h2><span class="checkmark">&#x2714;</span> User Saved!</h2>
            <h3><a href='user-edit.php'>+ Add New User</a> | <a href='user-list.php'><span class="listIcon">&#x2261;</span> View User List</a></h3>
        </div>
    </body>    
</html>