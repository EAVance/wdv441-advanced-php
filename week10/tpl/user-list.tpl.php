<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>User-List</title>
        <style>
            #container {
                padding:2%;
                background-color:#eeeeee;
                width:70%;
                margin:0 auto;
                min-width:500px;
                letter-spacing:.6px;
            }
            h2 {
                text-transform:uppercase;
                text-decoration:underline;
            }
            h2,h3 {
                text-align:center;
            }
            h3 {
                padding:3%;
            }
            h3 a,p a,input[type='submit'] {
                color:#648196;
                text-decoration:none;
            }
            h3 a:hover,p a:hover,input[type='submit']:hover {
                color:#3c4d5a;
                cursor:pointer;
            }
            p {
                width:500px;
                margin:2% auto;
                border-bottom:1px solid #c8c8c8;
            }
            p:first-of-type {
                padding-bottom:1%;
                margin-bottom:3%;
            }
            form {
                width:500px;
                margin:2% auto;
            }
            input,select {
                border:1px solid #dfdfdf;
                border-radius:5px;
                font-size:.9em;
            }
            .searchSort {
                text-transform:uppercase;
                font-weight:bold;
            }
            .sortBy {
                font-style:italic;
                padding-left:1%;
                font-size:1.1em;
            }
        </style>
    </head>
    <body>
        <div id="container">
            <h2>User List</h2>
            <h3><a href='user-edit.php'>+ Add New User</a></h3>
            <form action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" method="GET">
                <span class="searchSort">Search</span> 
                <select name="filterColumn">
                    <option value="username">Username</option>
                    <option value="userLevel">User Level</option>                  
                </select>
                &nbsp;<input type="text" name="filterText"/>
                &nbsp;<input type="submit" name="filter" value="Search"/>
            </form>
            <p><span class="searchSort">Sort By</span> 
                <span class="sortBy"> Username</span> &nbsp;-&nbsp;<a href="<?php echo $_SERVER['SCRIPT_NAME']; ?>?sortColumn=username&sortDirection=ASC">A&#x2191;</a>&nbsp;<a href="<?php echo $_SERVER['SCRIPT_NAME']; ?>?sortColumn=username&sortDirection=DESC">D&#x2193;</a>
                <span class="sortBy"> User Level</span> &nbsp;-&nbsp;<a href="<?php echo $_SERVER['SCRIPT_NAME']; ?>?sortColumn=userLevel&sortDirection=ASC">A&#x2191;</a>&nbsp;<a href="<?php echo $_SERVER['SCRIPT_NAME']; ?>?sortColumn=userLevel&sortDirection=DESC">D&#x2193;</a>
            </p>
            <?php 
                foreach ($userList as $user) 
                {
            ?>
            <p>
                <a href='user-edit.php?userID=<?php echo $user['userID']; ?>'>Edit </a> | <a href='user-view.php?userID=<?php echo $user['userID']; ?>'> View</a><br>
                <strong>Username: </strong><?php echo $user['username']; ?><br>  
                <strong>User Level: </strong><?php echo $user['userLevel']; ?><br>
            </p>
            <?php 
                } 
            ?>
        </div>
    </body>
</html>