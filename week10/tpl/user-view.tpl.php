<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>View User</title>
        <style>
            #container {
                padding:2% 2% 4% 2%;
                background-color:#eeeeee;
                width:60%;
                margin:0 auto;
                min-width:400px;
                letter-spacing:.6px;
            }
            h2 {
                text-decoration:underline;
                text-align:center;
                text-transform:uppercase;
            }
            h3 {
                padding:1%;
                text-align:center;
            }
            h3 a{
                color:#648196;
                text-decoration:none;
            }
            h2 a:hover{
                color:#3c4d5a;
            }
            .listIcon {
                font-size:1.4em;
            }
            img {
                border-radius: 50%;
                width:100px;
                text-align:center;
            }
        </style>
    </head>
    <body>
        <div id="container">
            <h2>View User</h2>
            <h3><a href='user-list.php'><span class="listIcon">&#x2261;</span> View User List</a></h3>
    <!--FIX IMAGE VIEW-->
            <p><img src="<?php echo '../public_html/images/user_'.$userDataArray['userID'].'.jpg'; ?>" alt="user profile image"></p>
            <h3>UserID: <?php echo echoValue($userDataArray, 'userID'); ?></h3>
            <h3>Username: <?php echo echoValue($userDataArray, 'username'); ?></h3>
            <h3>Password: <?php echo echoValue($userDataArray, 'password'); ?></h3>
            <h3>User Level: <?php echo echoValue($userDataArray, 'userLevel'); ?></h3>
        </div>
    </body>
</html>