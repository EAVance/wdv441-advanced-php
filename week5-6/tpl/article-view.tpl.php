<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Article</title>
        <style>
            #container {
                padding:2% 2% 4% 2%;
                background-color:#eeeeee;
                width:70%;
                margin:0 auto;
                min-width:500px;
                letter-spacing:.6px;
            }
            h2 {
                text-decoration:underline;
                text-align:center;
            }
            h3 {
                padding:1%;
                text-align:center;
                border-bottom:1px solid #c8c8c8;
            }
            h3:first-of-type {
                border:none;
            }
            p {
                width:500px;
                margin:4% auto;
            }
            h3 a{
                color:#648196;
                text-decoration:none;
            }
            h3 a:hover{
                color:#3c4d5a;
            }
            .listIcon {
                font-size:1.4em;
            }
        </style>
    </head>
    <body>
        <div id="container">
            <h3><a href='article-list.php'><span class="listIcon">&#x2261;</span> View Article List</a></h3>
            <h2><?php echo echoValue($articleDataArray, 'articleTitle'); ?></h2>
            <h3>Author: <em><?php echo echoValue($articleDataArray, 'articleAuthor'); ?></em> | Date: <em><?php echo echoValue($articleDataArray, 'articleDate'); ?></em></h3>
            <p><?php echo echoValue($articleDataArray, 'articleContent'); ?></p>
        </div>
    </body>
</html>