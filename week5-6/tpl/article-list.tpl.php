<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Article-List</title>
        <style>
            #container {
                padding:2%;
                background-color:#eeeeee;
                width:70%;
                margin:0 auto;
                min-width:500px;
                letter-spacing:.6px;
            }
            h2,h3 {
                text-align:center;
            }
            h3 {
                padding:3%;
            }
            h3 a,p a,input[type='submit'] {
                color:#648196;
                text-decoration:none;
            }
            h3 a:hover,p a:hover,input[type='submit']:hover {
                color:#3c4d5a;
                cursor:pointer;
            }
            p {
                width:500px;
                margin:2% auto;
                border-bottom:1px solid #c8c8c8;
            }
            p:first-of-type {
                padding-bottom:1%;
                margin-bottom:3%;
            }
            form {
                width:500px;
                margin:2% auto;
            }
            input,select {
                border:1px solid #dfdfdf;
                border-radius:5px;
                font-size:.9em;
            }
            .searchSort {
                text-transform:uppercase;
                font-weight:bold;
            }
            .sortBy {
                font-style:italic;
                padding-left:1%;
                font-size:1.1em;
            }
        </style>
    </head>
    <body>
        <div id="container">
            <h2>News Article List</h2>
            <h3><a href='article-edit.php'>+ Add New Article</a></h3>
            <form action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" method="GET">
                <span class="searchSort">Search</span> 
                <select name="filterColumn">
                    <option value="articleTitle">Article Title</option>
                    <option value="articleAuthor">Article Author</option>
                    <option value="articleDate">Article Date</option>
                    <option value="articleContent">Article Content</option>                    
                </select>
                &nbsp;<input type="text" name="filterText"/>
                &nbsp;<input type="submit" name="filter" value="Search"/>
            </form>
            <p><span class="searchSort">Sort By</span> 
                <span class="sortBy"> Title</span> &nbsp;-&nbsp;<a href="<?php echo $_SERVER['SCRIPT_NAME']; ?>?sortColumn=articleTitle&sortDirection=ASC">A&#x2191;</a>&nbsp;<a href="<?php echo $_SERVER['SCRIPT_NAME']; ?>?sortColumn=articleTitle&sortDirection=DESC">D&#x2193;</a>
                <span class="sortBy"> Author</span> &nbsp;-&nbsp;<a href="<?php echo $_SERVER['SCRIPT_NAME']; ?>?sortColumn=articleAuthor&sortDirection=ASC">A&#x2191;</a>&nbsp;<a href="<?php echo $_SERVER['SCRIPT_NAME']; ?>?sortColumn=articleAuthor&sortDirection=DESC">D&#x2193;</a>
                <span class="sortBy"> Date</span> &nbsp;-&nbsp;<a href="<?php echo $_SERVER['SCRIPT_NAME']; ?>?sortColumn=articleDate&sortDirection=ASC">A&#x2191;</a>&nbsp;<a href="<?php echo $_SERVER['SCRIPT_NAME']; ?>?sortColumn=articleDate&sortDirection=DESC">D&#x2193;</a>
            </p>
            <?php 
                foreach ($articleList as $article) 
                {
            ?>
            <p>
                <a href='article-edit.php?articleID=<?php echo $article['articleID']; ?>'>Edit </a> | <a href='article-view.php?articleID=<?php echo $article['articleID']; ?>'> View</a><br>
                <strong>Title: </strong><?php echo $article['articleTitle']; ?><br>  
                <strong>Author: </strong><?php echo $article['articleAuthor']; ?><br>
                <strong>Date: </strong><?php echo $article['articleDate']; ?><br>
                <strong>Content: </strong><?php echo $article['articleContent']; ?>
            </p>
            <?php 
                } 
            ?>
        </div>
    </body>
</html>