<?php
require_once('../inc/User.class.php');
require_once('../inc/helpers.php');

$user = new User();

$userDataArray = array();

// load the article if we have it
if (isset($_REQUEST['userID']) && $_REQUEST['userID'] > 0) 
{
    $user->load($_REQUEST['userID']);
    $userDataArray = $user->userData;
}

require_once('../tpl/user-view.tpl.php');
?>