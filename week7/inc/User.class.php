<?php

class User 
{
    var $userData = array();
    var $errors = array();

    var $db = null;

    function __construct() 
    {
        $this->db = new PDO('mysql:host=localhost;dbname=wdv441_2018;charset=utf8', 'wdv441', 'wdv4412018');      
    }
    
    function set($dataArray)
    {
        $this->userData = $dataArray;
    }
    
    function sanitize($dataArray)
    {
        $dataArray['username'] = filter_var($dataArray['username'], FILTER_SANITIZE_EMAIL);
        $dataArray['password'] = filter_var($dataArray['password'], FILTER_SANITIZE_STRING);

        return $dataArray;
    }
    
    function load($userID)
    {
        $isLoaded = false;

        // load from database                
        $stmt = $this->db->prepare("SELECT * FROM user_login WHERE userID = ?");
        $stmt->execute(array($userID));

        if ($stmt->rowCount() == 1) 
        {
            $dataArray = $stmt->fetch(PDO::FETCH_ASSOC);
            $this->set($dataArray);
            
            $isLoaded = true;
        }
                        
        return $isLoaded;
    }
    
    
    function save() 
    {
        $isSaved = false;
        
        // insert data to database if new user, else update user info in database
        // save data from userData property to database
        if (empty($this->userData['userID']))
        {
            $stmt = $this->db->prepare("INSERT INTO user_login (username, password, userLevel) VALUES (?, ?, ?)");
            $isSaved = $stmt->execute(array($this->userData['username'],$this->userData['password'],$this->userData['userLevel']));
                
            if ($isSaved) 
            {
                $this->userData['userID'] = $this->db->lastInsertId();
            }
        }
        else
        {
            $stmt = $this->db->prepare("UPDATE user_login SET username = ?, password = ?, userLevel = ? WHERE userID = ?");        
            $isSaved = $stmt->execute(array($this->userData['username'],$this->userData['password'],$this->userData['userLevel'],$this->userData['userID']));           
        }

        return $isSaved;
    }

    function duplicateUsernameCheck()
    {
        $isDuplicate = true;

        // if adding new user check to see if the database already has that Username(email) in use
        // else if updating existing user info, make sure they either keep their previous Username(email) or check that the updated Username(email) isn't already in use
        if (empty($this->userData['userID']))
        {
            $stmt = $this->db->prepare("SELECT username FROM user_login WHERE username = ?");
            $stmt->execute(array($this->userData['username']));

            if ($stmt->rowCount() == 0) 
            {
                $isDuplicate = false;
            }
            else
            {   
                $this->errors['username'] = "&#x2718; Username(email) already in use";
            }
        }
        else
        {
            $stmt = $this->db->prepare("SELECT userID, username FROM user_login WHERE userID = ? AND username = ?");
            $stmt->execute(array($this->userData['userID'],$this->userData['username']));

            if ($stmt->rowCount() >= 1) 
            {
                $isDuplicate = false;
            }
            else
            {   
                $this->errors['username'] = "&#x2718; Username(email) already in use";
            }
        }
        
        return $isDuplicate;
    }
    

    function validate()
    {
        $isValid = true;
        
        // validate the data elements in userData property

        //username is required & needs to be an email format
        if (empty($this->userData['username']))
        {
            $this->errors['username'] = "&#x2718; Required";
            $isValid = false;
        }
        else if (!filter_var($this->userData['username'], FILTER_VALIDATE_EMAIL)) 
        {
            $this->errors['username'] = "&#x2718; Invalid Email Format";
            $isValid = false;
        }  

        //password is required
        if (empty($this->userData['password']))
        {
            $this->errors['password'] = "&#x2718; Required";
            $isValid = false;
        }  

        return $isValid;
    }
    
    function verifyUser($dataArray) 
    {
        $foundUserID = null;

        $stmt = $this->db->prepare("SELECT userID FROM user_login WHERE username = ? AND password = ?"); 
		$stmt->execute(array($dataArray['username'], $dataArray['password']));
        
        //if user credentials match in db then fetch that userID
        if ($stmt->rowCount() == 1)
        {
            $result = $stmt->fetch(PDO::FETCH_ASSOC);
            $foundUserID = $result['userID'];
        }
        else 
        {
            $this->errors['invalidUser'] = "&#x2718; Invalid Username and/or Password";
        }
        
        return $foundUserID;
    }
    
//FIX
    function saveImage($imageFileData)
    {
        move_uploaded_file
        (           
            $imageFileData['tmp_name'], 
            dirname(__FILE__) . "/../public_html/images/user_" . $this->data[$this->keyField] . ".jpg"
        );
    }

    function getList($sortColumn = null, $sortDirection = null, $filterColumn = null, $filterText = null)
    {
        $userList = array();
        
        $sql = "SELECT * FROM user_login ";

        if (!is_null($filterColumn) && !is_null($filterText)) 
        {
            $sql .= " WHERE " . $filterColumn . " LIKE ?";
        }
        
        if (!is_null($sortColumn)) 
        {
            $sql .= " ORDER BY " . $sortColumn;
            
            if (!is_null($sortDirection))
            {
                $sql .= " " . $sortDirection;
            }
        }
        
        $stmt = $this->db->prepare($sql);
        
        if ($stmt)
        {
            $stmt->execute(array('%' . $filterText . '%'));
            
            $userList = $stmt->fetchAll(PDO::FETCH_ASSOC);
        }
                
        return $userList;        
    }
}
?>