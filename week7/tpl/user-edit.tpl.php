<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>User-Edit</title>
        <style>
            form {
                padding:2%;
                background-color:#eeeeee;
                width:70%;
                margin:0 auto;
                min-width:375px;
            }
            h2 {
                text-align:center;
            }
            p {
                width:350px;
                margin:0 auto;
            }
            #errors {
                text-align:center;
                color:#D06666;
            }
            input[type="text"],input[type="password"], select, option {
                display:block;
                width:350px;
                border:1px solid #dfdfdf;
                border-radius:5px;
                padding-left:5px;
                height:30px;
            }
            input::placeholder {
                font-style:italic;
                color:#d6d6d6;
            }
            #formBtns{
                text-align:center;
                margin-top:2%;
            }
            input[type="submit"]{
                font-size:1.3em;
                margin:1%;
                padding:2%;
            }
            .note {
                font-size:12px;
            }
            img {
                border-radius: 50%;
                width:50px;
            }
        </style>
    </head>
    <body>
        <form action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" method="post" enctype="multipart/form-data">
            <h2>Add/Edit User</h2>
            <p>Username (email): 
                <?php if (isset($userErrorsArray['username'])) 
                { ?>
                    <span id="errors"><?php echo $userErrorsArray['username'];?></span> 
                <?php } ?>
                <input type="text" name="username" placeholder="abc@yahoo.com" value="<?php echo echoValue($userDataArray, 'username'); ?>"/><br>
            </p>
            <p>Password: 
                <?php if (isset($userErrorsArray['password'])) 
                { ?>
                    <span id="errors"><?php echo $userErrorsArray['password'];?></span> 
                <?php } ?>
                <input type="password" name="password" value="<?php echo echoValue($userDataArray, 'password'); ?>"/><br>
            </p>
            <p>User Access Level:
                <select name="userLevel">
                    <option value="100" <?php echo ($userDataArray['userLevel'] == '100') ? "selected='selected'" : ''; ?>>Viewer - Level 1</option>
                    <option value="200" <?php echo ($userDataArray['userLevel'] == '200') ? "selected='selected'" : ''; ?>>User - Level 2</option>
                    <option value="300" <?php echo ($userDataArray['userLevel'] == '300') ? "selected='selected'" : ''; ?>>Publisher/Editor - Level 3</option>
                    <option value="400" <?php echo ($userDataArray['userLevel'] == '400') ? "selected='selected'" : ''; ?>>Administrator - Level 4</option>
                </select>
            </p>
    <!--FIX IMAGE UPLOAD-->
            <p>Profile Image:<br>
                <img src="images/<?php echo $userDataArray['userImg'];?>" alt="user profile image"/>
                <input type="file" name="photo"/>
                <input type="submit" name="Upload" value="Upload"/><br>
                <span class="note"><em> Only .jpg, .jpeg, .gif, .png formats allowed. Max size 5 MB</em></span>
            </p>
            <input type="hidden" name="userID" value="<?php echo echoValue($userDataArray, 'userID'); ?>"/>
            <div id="formBtns">
                <input type="submit" name="Save" value="Save"/>
                <input type="submit" name="Cancel" value="Cancel"/> 
            </div>           
        </form>          
    </body>
</html>