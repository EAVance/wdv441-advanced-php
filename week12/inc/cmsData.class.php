<?php
require_once('Base.class.php');

class cmsData extends Base 
{

    function __construct() 
    {
        parent::__construct();
        
        $this->tableName = "cms_data";
        $this->keyField = "cms_data_id";
        $this->columnNames = array
        (
           "page_title",
           "keywords",
           "header",
           "content",
           "url_key"
        );
    }

    function loadByKey($urlKey)
    {
        $isLoaded = false;

        // load from database                
        $stmt = $this->db->prepare("SELECT * FROM " . $this->tableName . " WHERE url_key =?");
        $stmt->execute(array($urlKey));

        if ($stmt->rowCount() == 1) 
        {
            $dataArray = $stmt->fetch(PDO::FETCH_ASSOC);
            //var_dump($dataArray);
            $this->set($dataArray);
            
            $isLoaded = true;
        }
        
        //var_dump($stmt->rowCount());
                
        return $isLoaded;
    }
    
    function santinize($data)
    {
        // sanitize data and trim whitespace from beginning and end of string
        $data['page_title'] = trim(filter_var($data['page_title'], FILTER_SANITIZE_STRING));
        $data['keywords'] = trim(filter_var($data['keywords'], FILTER_SANITIZE_STRING));
        $data['header'] = trim(filter_var($data['header'], FILTER_SANITIZE_STRING));
        $data['url_key'] = trim(filter_var($data['url_key'], FILTER_SANITIZE_STRING));
        $data['content'] = trim(filter_var($data['content'], FILTER_SANITIZE_STRING));

        return $data;
    }
        
    function validate()
    {
        $isValid = true;
        
        //page_title is required
        if (empty($this->data['page_title']))
        {
            $this->errors['page_title'] = "Page Title is Required";
            $isValid = false;
        }  

        //keywords are required
        if (empty($this->data['keywords']))
        {
            $this->errors['keywords'] = "Keywords are Required";
            $isValid = false;
        }  

        //header is required
        if (empty($this->data['header']))
        {
            $this->errors['header'] = "Header is Required";
            $isValid = false;
        }     
        
        //url_key is required
        if (empty($this->data['url_key']))
        {
            $this->errors['url_key'] = "URL Key is Required";
            $isValid = false;
        }  

        //content is required
        if (empty($this->data['content']))
        {
            $this->errors['content'] = "Content is Required";
            $isValid = false;
        }     
                        
        return $isValid;
    }

    function validateImg()
    {
        $isValid = true;

        //CMS content image validation
        if(isset($_FILES["cms_data_image"]) && $_FILES["cms_data_image"]["error"] == 0)
        {       
			$allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "png" => "image/png");
			$filename = $_FILES["cms_data_image"]["name"];
			$filetype = $_FILES["cms_data_image"]["type"];
			$filesize = $_FILES["cms_data_image"]["size"];
				
			// Verify file extension
			$ext = pathinfo($filename, PATHINFO_EXTENSION);
			if(!array_key_exists($ext, $allowed))
			{
				$isValid = false;
				$this->errors['cms_data_image'] = "&#x2718; Select a valid file format";
			}
				
			// Verify file size - 5MB maximum
			$maxsize = 5 * 1024 * 1024;
			if($filesize > $maxsize)
			{
				$isValid = false;
				$this->errors['cms_data_image'] = "&#x2718; File size is to large";
			}
				
			// Verify MIME type of the file
			if(!in_array($filetype, $allowed))
			{
				$isValid = false;
				$this->errors['cms_data_image'] = "&#x2718; Select a valid file format"; 
			}

        }
        else if($_FILES["cms_data_image"]["error"] == 1)
        {
			$isValid = false;
			$this->errors['cms_data_image'] = "&#x2718; " . $_FILES["cms_data_image"]["error"];
        }
        
        return $isValid;
    }

    //rename image and save image to cms_images folder
    function saveImage($imageFileData)
    {
        move_uploaded_file
        (           
            $imageFileData['tmp_name'], 
            dirname(__FILE__) . "/../public_html/cms_images/cms_data_" . $this->data[$this->keyField] . ".jpg"
        );
    }    

    //widget curl sessions
    function curlSession($url)
    {
        $curlSession = curl_init();

        curl_setopt($curlSession, CURLOPT_URL, $url);
        curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, 1);
        
        $widget = curl_exec($curlSession);
        curl_close($curlSession);

        return $widget;
    }
}
?>
