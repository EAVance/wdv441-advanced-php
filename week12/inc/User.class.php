<?php
require_once('Base.class.php');

class User extends Base
{

    function __construct() 
    {
        // still run parent functionality
        parent::__construct();
        
        $this->tableName = "user_login";
        $this->keyField = "userID";
        $this->columnNames = array
        (
           "username",
           "password",
           "userLevel"            
        );
    }
    
    
    function sanitize($data)
    {
        $data['username'] = filter_var($data['username'], FILTER_SANITIZE_EMAIL);
        $data['password'] = filter_var($data['password'], FILTER_SANITIZE_STRING);

        return $data;
    }
    

    function duplicateUsernameCheck()
    {
        $isDuplicate = true;

        // if adding new user check to see if the database already has that Username(email) in use
        // else if updating existing user info, make sure they either keep their previous Username(email) or check that the updated Username(email) isn't already in use
        if (empty($this->data['userID']))
        {
            $stmt = $this->db->prepare("SELECT username FROM user_login WHERE username = ?");
            $stmt->execute(array($this->data['username']));

            if ($stmt->rowCount() == 0) 
            {
                $isDuplicate = false;
            }
            else
            {   
                $this->errors['username'] = "&#x2718; Username(email) already in use";
            }
        }
        else
        {
            $stmt = $this->db->prepare("SELECT userID, username FROM user_login WHERE userID = ? AND username = ?");
            $stmt->execute(array($this->data['userID'],$this->data['username']));

            if ($stmt->rowCount() >= 1) 
            {
                $isDuplicate = false;
            }
            else
            {   
                $this->errors['username'] = "&#x2718; Username(email) already in use";
            }
        }
        
        return $isDuplicate;
    }
    

    function validate()
    {
        $isValid = true;
        
        // validate the data elements in data property

        //username is required & needs to be an email format
        if (empty($this->data['username']))
        {
            $this->errors['username'] = "&#x2718; Required";
            $isValid = false;
        }
        else if (!filter_var($this->data['username'], FILTER_VALIDATE_EMAIL)) 
        {
            $this->errors['username'] = "&#x2718; Invalid Email Format";
            $isValid = false;
        }  

        //password is required
        if (empty($this->data['password']))
        {
            $this->errors['password'] = "&#x2718; Required";
            $isValid = false;
        }  

        return $isValid;
    }

    
    function validateImg()
    {
        $isValid = true;

        //user profile image validation
        if(isset($_FILES["profileImg"]) && $_FILES["profileImg"]["error"] == 0)
        {       
			$allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "png" => "image/png");
			$filename = $_FILES["profileImg"]["name"];
			$filetype = $_FILES["profileImg"]["type"];
			$filesize = $_FILES["profileImg"]["size"];
				
			// Verify file extension
			$ext = pathinfo($filename, PATHINFO_EXTENSION);
			if(!array_key_exists($ext, $allowed))
			{
				$isValid = false;
				$this->errors['profileImg'] = "&#x2718; Select a valid file format";
			}
				
			// Verify file size - 5MB maximum
			$maxsize = 5 * 1024 * 1024;
			if($filesize > $maxsize)
			{
				$isValid = false;
				$this->errors['profileImg'] = "&#x2718; File size is to large";
			}
				
			// Verify MIME type of the file
			if(!in_array($filetype, $allowed))
			{
				$isValid = false;
				$this->errors['profileImg'] = "&#x2718; Select a valid file format"; 
			}

			// Check whether file exists before uploading it
			if(file_exists("../public_html/profile_images/" . $_FILES["profileImg"]["name"])){
				$isValid = false;
				$this->errors['profileImg'] = "&#x2718; " . $_FILES["profileImg"]["name"] . " already exists";
			}

        }
        else if($_FILES["profileImg"]["error"] == 1)
        {
			$isValid = false;
			$this->errors['profileImg'] = "&#x2718; " . $_FILES["profileImg"]["error"];
        }
        
        return $isValid;
    }

 //fix   
    function saveImage($imageFileData)
    {
        $isSaved = true;

        //upload image to profile_images folder
        if(!move_uploaded_file($imageFileData['tmp_name'], dirname(__FILE__) . "/../public_html/profile_images/" . $_FILES["profileImg"]["name"]))
        {
            $isSaved = false;
            $this->errors['profileImg'] = "&#x2718; Image upload error";
        }

        return $isSaved;
    }

    
    function verifyUser($data) 
    {
        $foundUserID = null;

        $stmt = $this->db->prepare("SELECT " . $this->keyField . " FROM " . $this->tableName . " WHERE username = ? AND password = ?"); 
		$stmt->execute(array($data['username'], $data['password']));
        
        //if user credentials match in db then fetch that userID
        if ($stmt->rowCount() == 1)
        {
            $result = $stmt->fetch(PDO::FETCH_ASSOC);
            $foundUserID = $result[$this->keyField];
        }
        else 
        {
            $this->errors['invalidUser'] = "&#x2718; Invalid Username and/or Password";
        }
        
        return $foundUserID;
    }

//fix
    function checkUserRights($user_id, $check_level)
    {
        $hasRights = false;
        
        if ($this->load($user_id))
        {
            $user_level = $this->data['userLevel'];            
            $hasRights = ($user_level >= $check_level);
        } 
        else 
        {
            $hasRights = ($check_level == 100);
        }
        
        
        return $hasRights;
    }
}
?>