<?php
require_once('Base.class.php');

class NewsArticles extends Base
{

    function __construct() 
    {
        parent::__construct();
        
        $this->tableName = "newsarticles";
        $this->keyField = "articleID";
        $this->columnNames = array
        (
           "articleTitle",
           "articleContent",
           "articleAuthor",
           "articleDate"
        );
    }

 
    function santinize($data)
    {
        $data['articleTitle'] = filter_var($data['articleTitle'], FILTER_SANITIZE_STRING);
        $data['articleContent'] = filter_var($data['articleContent'], FILTER_SANITIZE_STRING);
        $data['articleAuthor'] = filter_var($data['articleAuthor'], FILTER_SANITIZE_STRING);
        $data['articleDate'] = filter_var($data['articleDate'], FILTER_SANITIZE_NUMBER_INT);

        return $data;
    }
    
    
    function validate()
    {
        $isValid = true;
        
        // if an error, store to errors using column name as key
        
        // validate the data elements in data property

        //articleTitle is required
        if (empty($this->data['articleTitle']))
        {
            $this->errors['articleTitle'] = "Title is Required";
            $isValid = false;
        }  

        //articleContent is required
        if (empty($this->data['articleContent']))
        {
            $this->errors['articleContent'] = "Content is Required";
            $isValid = false;
        }  

        //articalAuthor is required
        if (empty($this->data['articleAuthor']))
        {
            $this->errors['articleAuthor'] = "Author is Required";
            $isValid = false;
        } 
        
        //articleDate is required, must be in yyyy-mm-dd format, and must be a real date
        $testDateArray = explode('-',$this->data['articleDate']);
        if (empty($this->data['articleDate']))
        {
            $this->errors['articleDate'] = "Date is Required";
            $isValid = false;
        }
        else if ( !preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/", $this->data['articleDate']) )
        {
            $this->errors['articleDate'] = "Invalid Format (Please use: yyyy-mm-dd)";
            $isValid = false;
        }
        else if ( !checkdate($testDateArray[1], $testDateArray[2], $testDateArray[0]) )
        {
            $this->errors['articleDate'] = "Date Doesn't Exist";
            $isValid = false;
        }   
       
        return $isValid;
    }
    

    function export($exportFile)
    {
        $success = true;
        
        $fileHandle = fopen($exportFile, "w");
        
        if ($fileHandle)
        {
            $articleList = $this->getList();
            
            var_dump($articleList);
            
            $headerRow = false;
            
            foreach ($articleList as $articleData)
            {
                if (!$headerRow) 
                {
                    fputcsv($fileHandle, array_keys($articleData));
                    $headerRow = true;
                }
                
                fputcsv($fileHandle, $articleData);
            }
            
            fclose($fileHandle);
        } 
        else 
        {
            $success = false;
        }
                
        return $success;
    }    
    

    function import($importFile)
    {
        $success = true;
        
        $fileHandle = fopen($importFile, "r");
        
        if ($fileHandle)
        {
            $headerColumns = array();
            
            // while i havent reached the end of the file, read more from it
            while (!feof($fileHandle))
            {
                $articleData = fgetcsv($fileHandle);
                
                if (empty($headerColumns))
                {
                    $headerColumns = $articleData;
                } 
                else 
                {
                    if (is_array($articleData))
                    {
                        // import the data
                        $articleData = array_combine($headerColumns, $articleData);
                        var_dump($articleData);

                        $this->set($articleData);
                        $this->save();                    
                    }
                }
                
            }
                        
            fclose($fileHandle);
        } 
        else 
        {
            $success = false;
        }
        
        return $success;
    }
    

    function saveImage($imageFileData)
    {
        move_uploaded_file
        (           
            $imageFileData['tmp_name'], 
            dirname(__FILE__) . "/../public_html/images/article_" . $this->data[$this->keyField] . ".jpg"
        );
    }
    
}
?>