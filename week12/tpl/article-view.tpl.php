<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Article</title>
        <link href="../public_html/styles/article-view-styles.css" rel="stylesheet">
    </head>
    <body>
        <div id="container">
            <h3><a href='article-list.php'><span class="listIcon">&#x2261;</span> View Article List</a></h3>
            <h2><?php echo echoValue($articleDataArray, 'articleTitle'); ?></h2>
            <h3>Author: <em><?php echo echoValue($articleDataArray, 'articleAuthor'); ?></em> | Date: <em><?php echo echoValue($articleDataArray, 'articleDate'); ?></em></h3>
            <p><?php echo echoValue($articleDataArray, 'articleContent'); ?></p>
        </div>
    </body>
</html>