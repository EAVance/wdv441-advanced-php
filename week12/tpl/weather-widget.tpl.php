<div class="weather-widget-container">
    <h2><span class="location">Ankeny, IA </span><br>Current Weather</h2>
    <h2><img src="weather_images/<?php echo $weatherWidget['wx_icon']; ?>"/> <?php echo $weatherWidget['wx_desc']; ?></h2>
    <h3><?php echo round($weatherWidget['temp_f']); ?> &#x2109; <br><span class="current-temp">Current Temp</span></h3>
    <p>Feels Like: <span class="other-info"><?php echo round($weatherWidget['feelslike_f']); ?> &#x2109;</span></p>
    <p>Humidity: <span class="other-info"><?php echo $weatherWidget['humid_pct']; ?> %</span></p>
    <p>Cloud Cover: <span class="other-info"><?php echo $weatherWidget['cloudtotal_pct']; ?> %</span></p>
    <p>Wind Speed: <span class="other-info"><?php echo round($weatherWidget['windspd_mph']); ?> mph</span></p>
    <p>Wind Direction: <span class="other-info"><?php echo $weatherWidget['winddir_compass']; ?></span></p>
</div>