<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>User-List</title>
        <link href="../public_html/styles/user-list-styles.css" rel="stylesheet">
    </head>
    <body>
        <div id="container">
            <h2>User List</h2>
            <h3><a href='user-edit.php'>+ Add New User</a></h3>
            <form action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" method="GET">
                <span class="searchSort">Search</span> 
                <select name="filterColumn">
                    <option value="username">Username</option>
                    <option value="userLevel">User Level</option>                  
                </select>
                &nbsp;<input type="text" name="filterText"/>
                &nbsp;<input type="submit" name="filter" value="Search"/>
            </form>
            <p><span class="searchSort">Sort By</span> 
                <span class="sortBy"> Username</span> &nbsp;-&nbsp;<a href="<?php echo $_SERVER['SCRIPT_NAME']; ?>?sortColumn=username&sortDirection=ASC">A&#x2191;</a>&nbsp;<a href="<?php echo $_SERVER['SCRIPT_NAME']; ?>?sortColumn=username&sortDirection=DESC">D&#x2193;</a>
                <span class="sortBy"> User Level</span> &nbsp;-&nbsp;<a href="<?php echo $_SERVER['SCRIPT_NAME']; ?>?sortColumn=userLevel&sortDirection=ASC">A&#x2191;</a>&nbsp;<a href="<?php echo $_SERVER['SCRIPT_NAME']; ?>?sortColumn=userLevel&sortDirection=DESC">D&#x2193;</a>
            </p>
            <?php 
                foreach ($userList as $user) 
                {
            ?>
            <p>
                <a href='user-edit.php?userID=<?php echo $user['userID']; ?>'>Edit </a> | <a href='user-view.php?userID=<?php echo $user['userID']; ?>'> View</a><br>
                <strong>Username: </strong><?php echo $user['username']; ?><br>  
                <strong>User Level: </strong><?php echo $user['userLevel']; ?><br>
            </p>
            <?php 
                } 
            ?>
        </div>
    </body>
</html>