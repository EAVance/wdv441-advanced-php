<div class="article-widget-container">
    <h2>Recent Articles</h2>
    <ul>
    <?php foreach ($articleList as $articleData)
    {?>
        <li><a href="article-view.php?articleID=<?php echo $articleData['articleID']; ?>"><strong><?php echo $articleData['articleTitle']; ?></strong> - <?php echo $articleData['articleDate']; ?></a></li>
    <?php } ?>
    </ul>
</div>