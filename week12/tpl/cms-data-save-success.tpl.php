<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>CMS Data-Saved</title>
        <link href="../public_html/styles/article-save-styles.css" rel="stylesheet">
    </head>
    <body>
        <div id="container">
            <h2>&#x2713; CMS Content Saved!</h2>
            <h3><a href="cms-data-list.php">View CMS Data List</a></h3>
        </div>
    </body>    
</html>