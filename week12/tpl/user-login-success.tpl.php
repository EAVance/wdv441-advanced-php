<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Login Success</title>
        <link href="../public_html/styles/user-login-success-styles.css" rel="stylesheet">
    </head>
    <body>
        <div id="container">
            <h2><span class="checkmark">&#x2714;</span> You are now logged in!</h2>
            <h3><a href='user-edit.php'>+ Add New User</a> | <a href='user-list.php'><span class="listIcon">&#x2261;</span> View User List</a></h3>
        </div>
    </body>    
</html>