<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Article-Saved</title>
        <link href="../public_html/styles/article-save-styles.css" rel="stylesheet">
    </head>
    <body>
        <div id="container">
            <h2>&#x2713; News Article Saved!</h2>
            <h3><a href='article-edit.php'>+ Add New Article</a> | <a href='article-list.php'><span class="listIcon">&#x2261;</span> View Article List</a></h3>
        </div>
    </body>    
</html>