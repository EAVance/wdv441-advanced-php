<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Article-Edit</title>
        <link href="../public_html/styles/article-edit-styles.css" rel="stylesheet">
    </head>
    <body>
        <form action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" method="post">
            <h2>News Article Form</h2>
            <p>Title: 
            <?php if (isset($articleErrorsArray['articleTitle'])) 
            { ?>
                <span id="errors"><?php echo $articleErrorsArray['articleTitle'];?></span> 
            <?php } ?>
                <input type="text" name="articleTitle" value="<?php echo echoValue($articleDataArray, 'articleTitle'); ?>"/><br>
            </p>
            <p>Content: 
            <?php if (isset($articleErrorsArray['articleContent'])) 
            { ?>
                <span id="errors"><?php echo $articleErrorsArray['articleContent'];?></span> 
            <?php } ?>
                <textarea name="articleContent"><?php echo echoValue($articleDataArray, 'articleContent'); ?></textarea><br>
            </p>
            <p>Author: 
            <?php if (isset($articleErrorsArray['articleAuthor'])) 
            { ?>
                <span id="errors"><?php echo $articleErrorsArray['articleAuthor'];?></span> 
            <?php } ?>
                <input type="text" name="articleAuthor" value="<?php echo echoValue($articleDataArray, 'articleAuthor'); ?>"/><br>
            </p>
            <p>Date: 
            <?php if (isset($articleErrorsArray['articleDate'])) 
            { ?>
                <span id="errors"><?php echo $articleErrorsArray['articleDate'];?></span> 
            <?php } ?>
                <input type="text" name="articleDate" placeholder="yyyy-mm-dd" value="<?php echo echoValue($articleDataArray, 'articleDate'); ?>"/><br>
            </p>
            <input type="hidden" name="articleID" value="<?php echo echoValue($articleDataArray, 'articleID'); ?>"/>
            <div id="formBtns">
                <input type="submit" name="Save" value="Save"/>
                <input type="submit" name="Cancel" value="Cancel"/> 
            </div>           
        </form>        
    </body>
</html>