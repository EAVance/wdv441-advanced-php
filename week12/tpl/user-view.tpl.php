<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>View User</title>
        <link href="../public_html/styles/user-view-styles.css" rel="stylesheet">
    </head>
    <body>
        <div id="container">
            <h2>View User</h2>
            <h3><a href="user-list.php"><span class="listIcon">&#x2261;</span> View User List</a></h3>
            <p><img src="../public_html/profile_images/<?php echo (empty($userDataArray['profileImg']) ? 'defaultProfileImg.png' : $userDataArray['profileImg']);?>" alt="user profile image"></p>
            <h3>UserID: <?php echo echoValue($userDataArray, 'userID'); ?></h3>
            <h3>Username: <?php echo echoValue($userDataArray, 'username'); ?></h3>
            <h3>User Level: <?php echo echoValue($userDataArray, 'userLevel'); ?></h3>
        </div>
    </body>
</html>