<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>CMS Data-Edit</title>
        <link href="../public_html/styles/article-edit-styles.css" rel="stylesheet">
    </head>
    <body>
        <form action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" method="post" enctype="multipart/form-data">

            <h2>CMS Content Add/Update</h2>

            <p>Page Title: 
            <?php if (isset($cmsErrorsArray['page_title'])) 
            { ?>
                <span id="errors"><?php echo $cmsErrorsArray['page_title'];?></span> 
            <?php } ?>
                <input type="text" name="page_title" value="<?php echo echoValue($cmsDataArray, 'page_title'); ?>"/><br>
            </p>

            <p>Keywords: 
            <?php if (isset($cmsErrorsArray['keywords'])) 
            { ?>
                <span id="errors"><?php echo $cmsErrorsArray['keywords'];?></span> 
            <?php } ?>
                <input type="text" name="keywords" value="<?php echo echoValue($cmsDataArray, 'keywords'); ?>"/><br>
            </p>

            <p>URL Key: 
            <?php if (isset($cmsErrorsArray['url_key'])) 
            { ?>
                <span id="errors"><?php echo $cmsErrorsArray['url_key'];?></span> 
            <?php } ?>
                <input type="text" name="url_key" value="<?php echo echoValue($cmsDataArray, 'url_key'); ?>"/><br>
            </p>

            <p>Header: 
            <?php if (isset($cmsErrorsArray['header'])) 
            { ?>
                <span id="errors"><?php echo $cmsErrorsArray['header'];?></span> 
            <?php } ?>
                <input type="text" name="header" value="<?php echo echoValue($cmsDataArray, 'header'); ?>"/><br>
            </p>

            <p>Content: 
            <?php if (isset($cmsErrorsArray['content'])) 
            { ?>
                <span id="errors"><?php echo $cmsErrorsArray['content'];?></span> 
            <?php } ?>
                <textarea name="content"><?php echo echoValue($cmsDataArray, 'content'); ?></textarea><br>
            </p>

            <p>Content Image:
            <?php if (isset($cmsErrorsArray['cms_data_image'])) 
            { ?>
                <span id="errors"><?php echo $cmsErrorsArray['cms_data_image'];?></span> 
            <?php } ?>
                <input type="file" name="cms_data_image"/><br>
            </p>
            
            <input type="hidden" name="cms_data_id" value="<?php echo echoValue($cmsDataArray, 'cms_data_id'); ?>"/>

            <div id="formBtns">
                <input type="submit" name="Save" value="Save"/>
                <input type="submit" name="Cancel" value="Cancel"/> 
            </div>             
        </form>        
    </body>
</html>