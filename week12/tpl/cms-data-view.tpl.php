<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="keyword" value="<?php echo (isset($cmsDataArray['keywords']) ? $cmsDataArray['keywords'] : ''); ?>"/>
        <title><?php echo (isset($cmsDataArray['page_title']) ? $cmsDataArray['page_title'] : ''); ?></title>        
        <link href="../public_html/styles/cms-data-view-styles.css" rel="stylesheet">
        <link href="../public_html/styles/newsarticles-widget-styles.css" rel="stylesheet">
        <link href="../public_html/styles/weather-widget-styles.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <div class="main-content-container">
                <?php if (file_exists(dirname(__FILE__) . "/../public_html/cms_images/cms_data_" . $cmsDataArray['cms_data_id'] . ".jpg"))
                { ?>
                    <div class="banner">
                        <img src="cms_images/cms_data_<?php echo $cmsDataArray['cms_data_id'] . ".jpg"; ?>"/>
                    </div>
                <?php }?>            
                <h1><?php echo (isset($cmsDataArray['header']) ? $cmsDataArray['header'] : ''); ?></h1>
                <div class="content">
                    <?php echo (isset($cmsDataArray['content']) ? $cmsDataArray['content'] : ''); ?>    
                </div>
            </div>  
            <div class="widget-container">  
                <?php echo $newsWidget; ?>
                <?php echo $weatherWidget; ?>
            </div>
        </div>
    </body>
</html>