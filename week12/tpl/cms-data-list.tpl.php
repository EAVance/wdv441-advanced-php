<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>CMS-List</title>
        <link href="../public_html/styles/user-list-styles.css" rel="stylesheet">
    </head>
    <body>
        <div id="container">
            <h2>CMS Data</h2>
            <h3><a href='cms-data-edit.php'>+ Add New CMS Data</a></h3>
            <form action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" method="GET">
                <span class="searchSort">Search</span> 
                <select name="filterColumn">
                    <option value="page_title">Page Title</option>
                    <option value="header">Header</option>
                    <option value="url_key">URL Key</option>             
                </select>
                &nbsp;<input type="text" name="filterText"/>
                &nbsp;<input type="submit" name="filter" value="Search"/>
            </form>
            <p><span class="searchSort">Sort By</span> <br>
                <span class="sortBy"> Page Title</span> &nbsp;-&nbsp;<a href="<?php echo $_SERVER['SCRIPT_NAME']; ?>?sortColumn=page_title&sortDirection=ASC">A&#x2191;</a>&nbsp;<a href="<?php echo $_SERVER['SCRIPT_NAME']; ?>?sortColumn=page_title&sortDirection=DESC">D&#x2193;</a>
                <span class="sortBy"> Header</span> &nbsp;-&nbsp;<a href="<?php echo $_SERVER['SCRIPT_NAME']; ?>?sortColumn=header&sortDirection=ASC">A&#x2191;</a>&nbsp;<a href="<?php echo $_SERVER['SCRIPT_NAME']; ?>?sortColumn=header&sortDirection=DESC">D&#x2193;</a>
                <span class="sortBy"> URL Key</span> &nbsp;-&nbsp;<a href="<?php echo $_SERVER['SCRIPT_NAME']; ?>?sortColumn=url_key&sortDirection=ASC">A&#x2191;</a>&nbsp;<a href="<?php echo $_SERVER['SCRIPT_NAME']; ?>?sortColumn=url_key&sortDirection=DESC">D&#x2193;</a>           
            </p>
            <?php 
                foreach ($cmsDataList as $cmsPageData) 
                {
            ?>
            <p>
                <a href="cms-data-edit.php?cms_data_id=<?php echo $cmsPageData['cms_data_id']; ?>">Edit </a> | <a href="cms-data-view.php?page=<?php echo $cmsPageData['url_key']; ?>"> View</a><br>
                <strong>Page Title: </strong><?php echo $cmsPageData['page_title']; ?><br>  
                <strong>Header: </strong><?php echo $cmsPageData['header']; ?><br>
                <strong>URL Key: </strong><?php echo $cmsPageData['url_key']; ?><br>
            </p>
            <?php 
                } 
            ?>
        </div>
    </body>
</html>