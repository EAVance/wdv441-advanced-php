<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>User Login</title>
        <link href="../public_html/styles/user-login-styles.css" rel="stylesheet">
    </head>
    <body>
        <form action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" method="post">
            <h2>Login</h2>
            <p class="invalidUser">
                <span id="errors"><?php echo echoValue($userErrorsArray, 'invalidUser');?></span> 
            </p>
            <p>Username (<em>email</em>): 
                <?php if (isset($userErrorsArray['username'])) 
                { ?>
                    <span id="errors"><?php echo $userErrorsArray['username'];?></span> 
                <?php } ?>
                <input type="text" name="username" placeholder="abc@yahoo.com" value="<?php echo echoValue($userDataArray, 'username'); ?>"/><br>
            </p>
            <p>Password: 
                <?php if (isset($userErrorsArray['password'])) 
                { ?>
                    <span id="errors"><?php echo $userErrorsArray['password'];?></span> 
                <?php } ?>
                <input type="password" name="password" value="<?php echo echoValue($userDataArray, 'password'); ?>"/><br>
            </p>
            <div id="formBtns">
                <input type="submit" name="Login" value="Login"/>
                <input type="submit" name="Cancel" value="Cancel"/> 
            </div>           
        </form>        
    </body>
</html>