<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>User-Edit</title>
        <link href="../public_html/styles/user-edit-styles.css" rel="stylesheet">
    </head>
    <body>
        <form action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" method="post" enctype="multipart/form-data">
            <h2>Add/Edit User</h2>
            <p>Username (email): 
                <?php if (isset($userErrorsArray['username'])) 
                { ?>
                    <span id="errors"><?php echo $userErrorsArray['username'];?></span> 
                <?php } ?>
                <input type="text" name="username" placeholder="abc@yahoo.com" value="<?php echo echoValue($userDataArray, 'username'); ?>"/><br>
            </p>
            <p>Password: 
                <?php if (isset($userErrorsArray['password'])) 
                { ?>
                    <span id="errors"><?php echo $userErrorsArray['password'];?></span> 
                <?php } ?>
                <input type="password" name="password" value="<?php echo echoValue($userDataArray, 'password'); ?>"/><br>
            </p>
            <p>User Access Level:
                <select name="userLevel">
                    <option value="100" <?php echo ($userDataArray['userLevel'] == '100') ? "selected='selected'" : ''; ?>>Viewer - Level 1</option>
                    <option value="200" <?php echo ($userDataArray['userLevel'] == '200') ? "selected='selected'" : ''; ?>>User - Level 2</option>
                    <option value="300" <?php echo ($userDataArray['userLevel'] == '300') ? "selected='selected'" : ''; ?>>Publisher/Editor - Level 3</option>
                    <option value="400" <?php echo ($userDataArray['userLevel'] == '400') ? "selected='selected'" : ''; ?>>Administrator - Level 4</option>
                </select>
            </p>
            <p>Profile Image:
                <?php if (isset($userErrorsArray['profileImg'])) 
                { ?>
                    <span id="errors"><?php echo $userErrorsArray['profileImg'];?></span> 
                <?php } ?>
                <br><input type="file" name="profileImg"/><img src="../public_html/profile_images/<?php echo (empty($userDataArray['profileImg']) ? 'defaultProfileImg.png' : $userDataArray['profileImg']);?>" alt="user profile image"/><br>
                <span class="note"><em> Only .jpg, .jpeg, .png formats allowed. Max size 5 MB</em></span>
            </p>
            <input type="hidden" name="userID" value="<?php echo echoValue($userDataArray, 'userID'); ?>"/>
            <div id="formBtns">
                <input type="submit" name="Save" value="Save"/>
                <input type="submit" name="Cancel" value="Cancel"/> 
            </div>           
        </form>          
    </body>
</html>