-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Apr 30, 2018 at 08:54 PM
-- Server version: 5.6.38
-- PHP Version: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wdv441_2018`
--

-- --------------------------------------------------------

--
-- Table structure for table `cms_data`
--

CREATE TABLE `cms_data` (
  `cms_data_id` int(11) NOT NULL,
  `page_title` varchar(500) NOT NULL,
  `keywords` varchar(150) NOT NULL,
  `header` varchar(500) NOT NULL,
  `content` mediumtext NOT NULL,
  `url_key` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cms_data`
--

INSERT INTO `cms_data` (`cms_data_id`, `page_title`, `keywords`, `header`, `content`, `url_key`) VALUES
(1, 'The Best Cupcakes', 'best cupcakes ever', 'The Best Cupcakes!', 'Cotton candy cupcake liquorice. Pie dragée tart. Caramels tart cupcake tart. Ice cream dessert gummi bears macaroon tootsie roll. Sesame snaps topping dessert croissant pie dragée gummies muffin. Brownie wafer chupa chups tiramisu chocolate cake powder jujubes. Dessert tart cake sesame snaps cookie candy canes sugar plum. Icing jelly-o bear claw.\r\n\r\nJelly-o dragée biscuit chocolate lemon drops. Liquorice bear claw toffee dessert marshmallow cupcake. Marshmallow donut bonbon. Tiramisu gingerbread dessert jelly-o jelly soufflé dessert tiramisu. Sweet roll jelly beans chocolate bar bonbon sugar plum.\r\n\r\nTiramisu pastry cookie chocolate cake sesame snaps sesame snaps cotton candy sweet roll donut. Donut pastry halvah. Jelly-o donut brownie lemon drops bear claw jelly. Tiramisu carrot cake jelly-o sugar plum bear claw jelly-o croissant. Jelly macaroon halvah carrot cake chocolate cupcake topping cotton candy marshmallow. Wafer dragée danish caramels danish.\r\n\r\nCotton candy macaroon muffin. Powder wafer cotton candy pastry. Cake bonbon candy canes jujubes pudding halvah lollipop candy pudding. Chocolate cake liquorice chupa chups candy canes jelly beans pastry lemon drops. Cake candy wafer dragée lemon drops lemon drops tiramisu croissant. Pudding caramels donut pie gingerbread tart. Dessert toffee gummies chocolate cheesecake tootsie roll chups sweet roll. ', 'best-cupcakes'),
(2, 'Cake Pops', 'delicious cake you have to try', 'Delicious Cake Pops', 'Cake halvah croissant halvah gummies bear claw chocolate cake donut. Ice cream gummies apple pie chocolate powder. Lemon drops chupa chups brownie cheesecake cake topping tart sweet roll jelly.\r\n\r\nTopping chocolate muffin chocolate. Gingerbread cotton candy donut tart tootsie roll gummies sugar plum oat cake. Carrot cake halvah pie. Fruitcake tootsie roll danish cotton candy cheesecake candy sugar plum toffee.\r\n\r\nChocolate soufflé oat cake tart powder jelly-o tart carrot cake. Marshmallow gingerbread ice cream toffee fruitcake topping. Carrot cake croissant toffee icing tart.\r\n\r\nSesame snaps pudding jujubes lollipop bear claw chocolate bar jelly-o. Tart cheesecake toffee soufflé bear claw jelly-o apple pie gingerbread. Danish gingerbread lemon drops muffin candy canes cotton candy apple pie pudding dessert.', 'cake-pops'),
(3, 'Easy Muffins', 'easy chocolate muffins', 'Easy Chocolate Muffins', 'Topping pastry icing sweet roll fruitcake dragée dragée jelly-o tiramisu. Lemon drops tootsie roll jujubes pudding apple pie chocolate bar biscuit jelly oat cake. Cake candy canes croissant toffee. Jelly beans tart marzipan muffin.\r\n\r\nGummies cheesecake sugar plum icing. Icing gingerbread fruitcake jujubes sesame snaps oat cake tiramisu cheesecake croissant. Jujubes oat cake bear claw marshmallow sesame snaps pudding. Cake cheesecake cotton candy chupa chups.\r\n\r\nLollipop pie candy canes. Sweet roll cupcake jujubes caramels dragée bonbon caramels chocolate cake liquorice. Gummies danish muffin tootsie roll.\r\n\r\nCandy canes marshmallow cake pudding halvah. Lollipop candy cookie marzipan cake powder sweet. Topping cupcake brownie sweet roll toffee jelly candy pie. Lemon drops carrot cake tootsie roll carrot cake brownie.', 'choc-muffins'),
(4, 'Mini Cookies', 'mini chocolate cookies that won&#39;t disappoint', 'Mini Chocolate Cookies', 'Cheesecake marshmallow jelly-o. Fruitcake icing sweet roll. Macaroon marzipan chupa chups muffin chupa chups cake. Icing cotton candy croissant.\r\n\r\nTopping chupa chups pastry jelly-o macaroon gingerbread sweet. Marshmallow cupcake macaroon halvah muffin topping cake. Liquorice cookie topping biscuit biscuit wafer.\r\n\r\nDessert caramels cotton candy croissant icing toffee jelly beans cookie. Wafer soufflé caramels cheesecake tart marshmallow pie. Wafer tiramisu croissant lemon drops tiramisu carrot cake dragée. Sweet gummi bears tootsie roll.\r\n\r\nSweet icing caramels donut jelly sweet. Caramels muffin ice cream jelly beans bonbon tootsie roll. Cupcake muffin jelly-o.', 'mini-choc-cookies'),
(5, 'Awesome Cheesecake', 'Awesome Homemade Blueberry Cheesecake', 'Homemade Blueberry Cheesecake', 'Candy cheesecake croissant. Sugar plum cake powder cookie chupa chups soufflé sesame snaps icing. Cheesecake gingerbread sweet roll. Soufflé gummies cake marshmallow cake chocolate.\r\n\r\nHalvah jujubes sugar plum bonbon dessert gingerbread muffin sweet roll sesame snaps. Candy canes icing sugar plum toffee gummies. Cake cookie lemon drops cake sesame snaps fruitcake dragée. Lemon drops bonbon caramels sesame snaps chocolate bar jujubes carrot cake chocolate bar.\r\n\r\nMarzipan carrot cake chocolate cake cake chupa chups pie apple pie soufflé tiramisu. Halvah powder cupcake. Jelly macaroon sugar plum gummi bears jelly wafer halvah macaroon gummies. Macaroon powder jujubes cotton candy caramels.\r\n\r\nOat cake cookie jujubes sweet roll gummi bears soufflé brownie. Gummi bears sweet cake. Jelly candy soufflé jelly-o.', 'blueberry-cheesecake');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cms_data`
--
ALTER TABLE `cms_data`
  ADD PRIMARY KEY (`cms_data_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cms_data`
--
ALTER TABLE `cms_data`
  MODIFY `cms_data_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
