<?php
require_once('../inc/User.class.php');
require_once('../inc/helpers.php');

$user = new User();

$userDataArray = array();
$userErrorsArray = array();

// load the user if we have it
if (isset($_REQUEST['userID']) && $_REQUEST['userID'] > 0) 
{
    $user->load($_REQUEST['userID']);
    $userDataArray = $user->data;
}

// if cancel reload page
if (isset($_POST['Cancel'])) 
{
    header("location: user-list.php");
    exit;
}

// sanitize, validate, check for username duplicates then save data
if (isset($_POST['Save']))
{
    $_POST['profileImg'] = $_FILES["profileImg"]["name"];
    $userDataArray = $_POST;

    // sanitize
    $userDataArray = $user->sanitize($userDataArray);
    $user->set($userDataArray);

    // validate
    if ($user->validate() && $user->validateImg())
    {
        // check for duplicate Username(email) already in use
        if (!$user->duplicateUsernameCheck())
        {
            // save data to database & save uploaded user profile image to profile_images folder
            if ($user->save())
            {
                $user->saveImage($_FILES['profileImg']);
                header("location: user-save-success.php");
                exit;
            }
            else
            {
                $userErrorsArray = $user->errors;
            }
        }
        else
        {
            $userErrorsArray = $user->errors;
        }
    }
    else
    {
        $userErrorsArray = $user->errors;
        $userDataArray = $user->data;
    }
}

require_once('../tpl/user-edit.tpl.php');
?>
