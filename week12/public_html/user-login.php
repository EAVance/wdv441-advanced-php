<?php
require_once('../inc/User.class.php');
require_once('../inc/helpers.php');

$user = new User();

$userDataArray = array();
$userErrorsArray = array();

// if cancel reload login page
if (isset($_POST['Cancel'])) 
{
    header("location: user-login.php");
    exit;
}

// if login, sanitize, validate, verify user credentials and start session
if (isset($_POST['Login']))
{
    $userDataArray = $_POST;

    // sanitize
    $userDataArray = $user->sanitize($userDataArray);
    $user->set($userDataArray);

    // validate 
    if ($user->validate())
    {
        // verify user credentials
        $foundUserID = $user->verifyUser($userDataArray);
        // if user is verified and not null then start session
        if(!is_null($foundUserID))
        {
            session_start();
            $_SESSION['user_id'] = $foundUserID;

            header('location: user-login-success.php');
            exit;
        }
        else 
        {
            $userErrorsArray = $user->errors;
        }
    }
    else
    {
        $userErrorsArray = $user->errors;
        $userDataArray = $user->data;
    }
}

require_once('../tpl/user-login.tpl.php');
?>
