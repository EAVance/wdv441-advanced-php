<?php
    //uses weather unlocked api to get the current weather data for 50021
    $curlSession = curl_init();

    if ($curlSession) 
    {
        $currentWeatherUrl = "http://api.weatherunlocked.com/api/current/us.50021?app_id=2c9df5dc&app_key=9cec47868fa394bed63695545fddac2f";

        curl_setopt($curlSession, CURLOPT_URL, $currentWeatherUrl);
        curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlSession, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curlSession, CURLOPT_HTTPHEADER, array('Accept: application/json',));
        
        $weatherWidget = curl_exec($curlSession);
        
        //var_dump($weatherWidget);
        //var_dump(curl_error($curlSession));

        //turns returned data from json to php
        $weatherWidget = json_decode($weatherWidget, true);

        //var_dump($weatherWidget);

        curl_close($curlSession);
    }

    require_once("../tpl/weather-widget.tpl.php");
?>