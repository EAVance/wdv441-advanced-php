<?php
require_once('../inc/cmsData.class.php');

$cmsData = new cmsData();

$cmsDataArray = array();

if (isset($_REQUEST['page'])) 
{
    $cmsData->loadByKey($_REQUEST['page']);
    $cmsDataArray = $cmsData->data;

    //get the widgets
    $newsWidget = $cmsData->curlSession("http://localhost:8888/wdv441ADVphp/week12/public_html/article-widget.php?numberOfArticles=5&sortColumn=articleTitle&sortDirection=asc");
    $weatherWidget = $cmsData->curlSession("http://localhost:8888/wdv441ADVphp/week12/public_html/weather-widget-current.php");
}
else
{
    header("location: cms-data-list.php");
    exit;
}

require_once('../tpl/cms-data-view.tpl.php');
?>